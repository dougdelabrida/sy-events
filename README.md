### Development guide

Requirements 

* [NodeJS](https://nodejs.org/en/download/) V6 (or higher) - For development
* [Git](https://git-scm.com/downloads) - For version control
* yarn

Open your terminal and follow theses steps

1 - Clone this repo into a folder:

`git clone https://dougdelabrida@bitbucket.org/dougdelabrida/sy-events.git`
after cloning, navigate to the new folder `cd sy-events`

2 - Install the dependencies:

`yarn install`

### Development
Once having followed the Installation Guide, you're able to develop

In the project's folder run `yarn start` in your terminal

If everything is ok the app will be available at `http://localhost:8080/events`

### TODO
* Form validations
* Notifications
* Production build script
* Edit event
* Image upload
