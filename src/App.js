import React from 'react'
import Routes from './Routes'
import PrimaryLayout from './layouts/PrimaryLayout'
import { Provider } from 'react-redux'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import store from './store'

import './App.scss'

const storeInstace = store()

export default () => (
  <Provider store={storeInstace}>
    <BrowserRouter>
      <div>
        <Switch>
          <Routes path='/events' component={PrimaryLayout} />
        </Switch>
      </div>
    </BrowserRouter>
  </Provider>
)
