import React from 'react'
import { Route } from 'react-router-dom'

export default (props) => {
  const { component: Component, ...rest } = props

  return (
    <Route {...rest} render={props => {
      // Conditions can be added here
      return <Component {...props} />
    
    }} />
  )
}
