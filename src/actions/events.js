export const FETCH_REQUEST = 'FETCH_REQUEST'
export const RECEIVE_EVENTS = 'RECEIVE_EVENTS'

const API_URI = 'https://powerful-waters-90985.herokuapp.com/api'

export const fetchEvents = () => {
  return dispatch => {
    dispatch({ type: FETCH_REQUEST})

    fetch(`${API_URI}/events`, { method: 'GET' })
      .then(response => response.json())
      .then(result => {

        dispatch({
          type: RECEIVE_EVENTS,
          result
        })

      }
    )
  }
}

export const addEvent = (event) => {
  fetch(`${API_URI}/events`,
  {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(event)
  }).then(response => {
    if (response.status === 201) {
      response.json().then(data => {
        alert('Evento publicado') // alert? =/
      })
    }
  })
}
