import React from 'react'
import PropTypes from 'prop-types'

import './Modal.scss'

export const Modal = (props) => {
  const { title, isOpen, onRequestClose, children, actions } = props

  return (
    <div className={`sy-modal-wrapper ${isOpen && 'opened'}`}>
      {isOpen && 
      
      <div className='sy-modal'>

        <div className='sy-modal__title'>
          {title}
        </div>
        <div className='sy-modal__content'>
          {children}
        </div>
        <div className='sy-modal__actions'>
          {actions && actions}
        </div>
      </div>}

      <div className='sy-modal-overlay' onClick={onRequestClose}></div>
    </div>
  )
}

Modal.propTypes = {
  title: PropTypes.string,
  isOpen: PropTypes.bool.isRequired,
  onRequestClose: PropTypes.func.isRequired,
  children: PropTypes.any,
  actions: PropTypes.element
}

export default Modal
