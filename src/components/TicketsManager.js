import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Button from './UI/Button';
import Modal from './Modal'
import TextField from './UI/TextField'
import Checkbox from './UI/Checkbox'

import './TicketsManager.scss'

const ticket = {
  type: '',
  price: '',
  free: false,
  units: 0,
  salesStart: '',
  startTime: '',
  salesEnd: '',
  endTime: ''
}

export default class TicketsManager extends Component {
  static propTypes = {
    onChange: PropTypes.func,
    tickets: PropTypes.array.isRequired
  }

  state = {
    open: false,
    ticket: Object.assign({}, ticket)
  }

  onRequestOpen() {
    this.setState({open: true})
  }

  onRequestClose() {
    this.setState({open: false, ticket: ticket})
  }

  handleInputs({target}) {
    this.state.ticket[target.name] = target.value
    this.setState(this.state)
  }

  handleFree({target}) {
    this.state.ticket.free = target.checked
    this.setState(this.state)
  }

  create() {
    this.props.tickets.push(Object.assign({}, this.state.ticket))
    this.props.onChange(this.props.tickets)
    this.onRequestClose()
  }

  render() {
    const { open, ticket } = this.state
    const { tickets } = this.props

    const getEmpty = () => (
      <div className='list-empty'>
        <p>Você ainda não possui ingressos criados.</p>
        <Button text='Criar Ingresso' color='orange' size='small' onClick={() => this.onRequestOpen()}/>
      </div>
    )

    const getList = (tickets) => (
      <table cellSpacing={0}>
        <thead border={1}>
          <tr>
            <th>Tipo</th>
            <th>Data Início</th>
            <th>Data Término</th>
            <th>Valor</th>
            <th>Qtde.</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {tickets.map((ticket, i) => (
            <tr key={i}>
              <td>{ticket.type}</td>
              <td>{ticket.salesStart}</td>
              <td>{ticket.salesEnd}</td>
              <td>{ticket.price}</td>
              <td>{ticket.units}</td>
              <td></td>
            </tr>
          ))}
        </tbody>
      </table>
    )
    

    return (
      <div>
        {tickets.length === 0 && getEmpty()}
        {tickets.length > 0 && getList(tickets)}
        <Modal
          title='Criar Ingresso'
          isOpen={open}
          onRequestClose={() => this.onRequestClose()}
        >
          <div className='add-form'>
            <TextField
              floatingLabelText='Tipo de ingresso'
              name='type'
              value={ticket.type}
              onChange={e => this.handleInputs(e)}
            />
            <div className='two-items'>
              <TextField
                floatingLabelText='Preço'
                name='price'
                value={ticket.price}
                disabled={ticket.free}
                onChange={e => this.handleInputs(e)}
              />

              <Checkbox
                label='Grátis'
                name='free'
                checked={ticket.free}
                onChange={e => this.handleFree(e)}
              />
            </div>

            <div className='two-items'>
              <TextField
                floatingLabelText='Quantidade'
                type='number'
                name='units'
                value={ticket.units}
                onChange={e => this.handleInputs(e)}
              />
            </div>
            
            <div className='two-items'>
              <TextField.date
                floatingLabelText='Início Vendas'
                name='salesStart'
                value={ticket.salesStart}
                onChange={e => this.handleInputs(e)}
              />

              <TextField.time
                floatingLabelText='Hora Início'
                name='startTime'
                value={ticket.startTime}
                onChange={e => this.handleInputs(e)}
              />
            </div>

            <div className='two-items'>
              <TextField.date
                floatingLabelText='Término Vendas'
                name='salesEnd'
                value={ticket.salesEnd}
                onChange={e => this.handleInputs(e)}
              />

              <TextField.time
                floatingLabelText='Hora Término'
                name='endTime'
                value={ticket.endTime}
                onChange={e => this.handleInputs(e)}
              />
            </div>

            <div className='two-items actions'>
              <Button color='flat' text='Cancelar' onClick={() => this.onRequestClose()} />
              <Button color='orange' text='Criar Ingresso' onClick={() => this.create()} />
            </div>
          </div>
        </Modal>
      </div>
    )
  }
}

