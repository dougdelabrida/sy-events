import React from 'react'
import propTypes from 'prop-types'

import './Button.scss'

const Button = (props) => {
  const { text, type, color, size, onClick } = props

  const colors = {
    undefined: '',
    secondary: 'sy-button--secondary',
    orange: 'sy-button--orange',
    flat: 'sy-button--flat'
  }

  const sizes = {
    undefined: '',
    small: 'sy-button--small'
  }

  return (
    <button
      type={type}
      className={`sy-button ${colors[color]} ${sizes[size]}`}
      onClick={onClick}
      >
      <span>{text}</span>
    </button>
  )
}

Button.defaultProps = {
  type: 'button'
}

Button.propTypes = {
  text: propTypes.string.isRequired,
  onClick: propTypes.func
}

export default Button
