import React from 'react'
import './Card.scss'

export default ({title, children, className}) => (
  <div className={`card ${className ? className : ''}`}>
      {title && <div className='card__title'>{title}</div>}
      <div className='card__content'>
        {children}
      </div>
  </div>
)
