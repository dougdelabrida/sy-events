import React, { Component } from 'react'
import PropTypes from 'prop-types'

import './Checkbox.scss'

export default class Checkbox extends Component {
  static propTypes = {
    label: PropTypes.string
  }

  render() {
    const { label, ...rest } = this.props
    return (
      <div className="checkbox">
        <input type="checkbox" {...rest} />
        <label>{label}</label>
      </div>
    )
  }
}
