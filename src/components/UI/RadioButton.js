import React, { Component } from 'react'
import PropTypes from 'prop-types'

import './RadioButton.scss'

export default class RadioButton extends Component {
  static propTypes = {
    label: PropTypes.string
  }

  render() {
    const { label, ...rest } = this.props
    return (
      <div className="md-radio">
        <input type="radio" {...rest} />
        <label>{label}</label>
      </div>
    )
  }
}
