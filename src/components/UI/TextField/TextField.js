import React, { Component } from 'react'
import PropTypes from 'prop-types'

import SVGDate from 'react-svg-loader!../../../assets/ic-date.svg'
import SVGTime from 'react-svg-loader!../../../assets/ic-time.svg'

import './TextField.scss'

export default class TextField extends Component {

  static defaultProps = {
    errorMessage: '',
    value: ''
  }
  
  static propTypes = {
    floatingLabelText: PropTypes.string,
    name: PropTypes.string.isRequired,
    errorMessage: PropTypes.string,
    onChange: PropTypes.func,
    required: PropTypes.bool,
    value: PropTypes.any,
    type: PropTypes.string
  }

  state = {
    active: this.props.value !== '',
    fieldValue: this.props.value,
    invalid: this.props.errorMessage !== ''
  }

  handleField = ({value}) => {
    this.setState({
      fieldValue: value
    })
  }

  checkStatus = () => {
    this.setState({
      active: this.refs.input.value !== '',
    })
  }

  render() {
    const { floatingLabelText, type, required, errorMessage, onChange, value, ...rest } = this.props
    const { active, fieldValue, invalid } = this.state

    return (
      <div className={`text-field-wrapper ${invalid ? 'invalid' : ''}`}>
        <div className='text-field'>
          <input
            ref='input'
            type={type}
            value={value || fieldValue}
            className={active ? 'active' : ''}
            onChange={onChange || (e => this.handleField(e.target))}
            onBlur={() => this.checkStatus()}
            {...rest}
          />

          <label>{floatingLabelText}</label>

          <div className='text-field__underline'></div>

          {required && <span className='text-field__required'>*</span>}
        </div>

        {errorMessage && <div className='error-message'>{errorMessage}</div>}
      </div>
    )
  }
}

TextField.date = ({...rest}) => {
  return (
    <div className='text-field-modifier'>
      <SVGDate className='text-field-modifier__icon' width={18} />
      <TextField {...rest} required />
    </div>
  )
}

TextField.time = ({...rest}) => {
  return (
    <div className='text-field-modifier'>
      <SVGTime className='text-field-modifier__icon' width={18} />
      <TextField {...rest} required />
    </div>
  )
}
