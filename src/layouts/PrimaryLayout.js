import React from 'react'
import { Switch, Route } from 'react-router-dom'
import EventList from '../pages/EventList'
import AddEvent from '../pages/AddEvent'
import Header from './components/Header'
import HeaderTitle from './components/HeaderTitle'

import './PrimaryLayout.scss'

export default (props) => {
  const { match } = props

  return (
    <div className='primary-layout'>
      <Header />
      <HeaderTitle />
      <main>
        <Switch>
          <Route path={`${match.path}`} exact component={EventList} />
          <Route path={`${match.path}/add`} component={AddEvent} />
        </Switch>
      </main>
    </div>
  )
}

