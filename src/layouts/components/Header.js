import React from 'react'
import Button from '../../components/UI/Button';
import { NavLink } from 'react-router-dom';
import withRouter from 'react-router-dom/withRouter';

import './Header.scss'

const Header = ({ match }) => {
  return (
    <div className='app-header'>
      <div className='app-header__nav'>
      
        <NavLink to={`${match.path}`}>
          <Button color='secondary' text='Meus Eventos' />
        </NavLink>

        <NavLink to={`${match.path}/add`}>
          <Button text='+ Criar Evento' />
        </NavLink>

      </div>
    </div>
  )
}

export default withRouter(Header)
