import React from 'react'
import { Switch, Route, withRouter } from 'react-router-dom'

import './HeaderTitle.scss'

const HeaderTitle = ({match}) => {
  return (
    <div className='header-title'>
      <div className="container">

        <Switch>
          <Route path={`${match.path}`} exact>
            <h1>Meus Eventos</h1>
          </Route>
          <Route path={`${match.path}/add`}>
            <h1>Criar Evento</h1>
          </Route>
        </Switch>

      </div>
    </div>
  )
}

export default withRouter(HeaderTitle)
