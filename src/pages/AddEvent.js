import React, { Component } from 'react'
import Card from '../components/UI/Card'
import TextField from '../components/UI/TextField'
import Button from '../components/UI/Button'
import TicketsManger from '../components/TicketsManager'
import RadioButton from '../components/UI/RadioButton'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { addEvent } from '../actions/events'

import SVGPublish from 'react-svg-loader!../assets/ic-publish-white.svg'

import './AddEvent.scss'

const event = {
  name: '',
  image: {
    type: '',
    url: '',
  },
  period: {
    startDate: '',
    timeStart: '',
    endDate: '',
    timeEnd: ''
  },
  tickets: []
}

class AddEvent extends Component {
  state = {
    event: Object.assign({}, event)
  }

  handleName({target}) {
    this.state.event.name = target.value
    this.setState(this.state)
  }

  handlePeriod({target}) {
    this.state.event.period[target.name] = target.value
    this.setState(this.state)
  }

  handleTickets(tickets) {
    this.state.event.tickets = tickets
    this.setState(this.state)
  }

  handleCreate(e) {
    e.preventDefault()
    this.props.actions.addEvent(this.state.event)
  }

  render() {
    const { event } = this.state

    return (
      <div className='add-event-wrapper'>
        <form onSubmit={e => this.handleCreate(e)}>
          <div className='add-event container'>
            <Card title='1. Nome e imagem do evento'>
              <div style={{width: 700}}>

                <TextField
                  name="eventName"
                  floatingLabelText='Nome do evento'
                  onChange={e => this.handleName(e)}
                  value={event.name}
                />

                <div className='add-event__image-type'>
                  <label>Inserir imagem:</label>
                  <RadioButton label='Banner' value='banner' name='imageType' />
                  <RadioButton label='Logo' value='logo' name='imageType' />
                </div>
                
                <div>
                  <div className='upload-area'>

                  </div>
                  <p>
                    A imagem escolhida deve estar em formato JPG, GIF, ou PNG e ter no máximo 2MB. A dimensão recomendada é de 1200 x 444 pixels. Imagens com altura ou largura diferentes destas podem ser redimensionadas.
                  </p>
                </div>
              </div>
            </Card>

            <Card title='2. Quando?'>
              <div className='add-event__when'>
                <TextField.date
                  name="startDate"
                  floatingLabelText='Data início'
                  value={event.period.startDate}
                  onChange={e => this.handlePeriod(e)}
                />

                <TextField.time
                  name="timeStart"
                  floatingLabelText='Hora início'
                  value={event.period.timeStart}
                  onChange={e => this.handlePeriod(e)}
                />

                <TextField.date
                  name="endDate"
                  floatingLabelText='Data término'
                  value={event.period.endDate}
                  onChange={e => this.handlePeriod(e)}
                />

                <TextField.time
                  name="timeEnd"
                  floatingLabelText='Hora término'
                  value={event.period.timeEnd}
                  onChange={e => this.handlePeriod(e)}
                />
              </div>
            </Card>

            <Card title='3. Ingressos'>
              <TicketsManger
                tickets={event.tickets}
                onChange={tickets => this.handleTickets(tickets)}
              />
            </Card>

          </div>

          <Card className='add-event-wrapper__publish'>
            <Button type='submit' color='orange' icon={SVGPublish} text='Publicar Evento' />
          </Card>
        </form>
      </div>
    )
  }
}

function mapStateToProps(state, props) {
  return {
    events: state.events
  }
}

function mapDispatchToProps(dispatch) {
  const eventsActions = {addEvent}

  return {
      actions: bindActionCreators(eventsActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddEvent)
