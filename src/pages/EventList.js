import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
import Button from '../components/UI/Button'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { fetchEvents } from '../actions/events'

import './EventList.scss'
import SVGCalendar from 'react-svg-loader!../assets/icon-calendar.svg'
import Card from '../components/UI/Card/Card'

class EventList extends Component {
  componentDidMount() {
    this.props.actions.fetchEvents()
  }

  render() {
    const { data, isFetching } = this.props.events

    if (isFetching) {
      return <span>Carregando...</span>
    }

    if (data.length === 0) {
      return (
        <div className='list-empty list-empty--event'>
          <SVGCalendar width={58} />

          <p>Bem-vindo(a). Você ainda não possui eventos.</p>

          <NavLink to='/events/add'>
            <Button color='orange' size='small' text='Criar Novo Evento' />
          </NavLink>
        </div>
      )
    }

    return (
      <div className='event-list container'>
        <Card>
          Minha Lista de eventos
          <table cellSpacing={0}>
            <thead border={1}>
              <tr>
                <th>Evento</th>
                <th>Data Início</th>
                <th>Data Término</th>
                <th>Ingresso</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {data.map((item, i) => (
                <tr key={i}>
                  <td>{item.name}</td>
                  <td>{item.period.startDate}</td>
                  <td>{item.period.endDate}</td>
                  <td>{item.tickets[0].units}</td>
                  <td>a b</td>
                </tr>
              ))}
            </tbody>
          </table>
        </Card>
      </div>
    )
  }
}

function mapStateToProps(state, props) {
  return {
    events: state.events
  }
}

function mapDispatchToProps(dispatch) {
    const eventsActions = {fetchEvents}

    return {
        actions: bindActionCreators(eventsActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(EventList)
