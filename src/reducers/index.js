import events from './events'
import { combineReducers } from 'redux'

const rootReducer = combineReducers({
  events
})

export default rootReducer
