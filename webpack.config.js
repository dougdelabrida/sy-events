const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const prod = JSON.parse(process.env.NODE_ENV === 'prod');

module.exports = {
  entry: './src/main.js',
  output: {
    publicPath: '/',
    path: path.resolve('dist'),
    filename: prod ? 'app-[hash].min.js' : 'app.bundle.js'
  },
  module: {
    loaders: [
      { test: /\.js$/, loader: 'babel-loader', exclude: /node_modules/ },
      { test: /\.jsx$/, loader: 'babel-loader', exclude: /node_modules/ },
      { test: /\.scss$/,
        use: ['css-hot-loader'].concat(ExtractTextPlugin.extract({
          use: [
            {
              loader: 'css-loader'
            },
            {
              loader: 'sass-loader',
              options: {
                sourceMap: true,
                data: '@import "./src/components/shared/_variables";'
              }
            }
          ]
        }))
      }
    ]
  },
  plugins: [
    prod ? new webpack.optimize.UglifyJsPlugin({
      compress: { warnings: false }
    }) : () => {},
    prod ? new webpack.DefinePlugin({
      'process.env': { 
         NODE_ENV: JSON.stringify('production') 
       }
    }) : () => {},
    new HtmlWebpackPlugin({
      template: './public/index.html',
      filename: 'index.html',
      inject: 'body'
    }),
    new ExtractTextPlugin('style.css')
  ]
}
